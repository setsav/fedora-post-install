#!/bin/sh

sudo echo -e "#Added for speed: \nfastestmirror=True \nmax_parallel_downloads=10 \ndefaultyes=True \nkeepcache=True" >> /etc/dnf/dnf.conf &&

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo &&
sudo dnf install \
  https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm &&
sudo dnf install \
  https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm &&
sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel &&
sudo dnf install lame\* --exclude=lame-devel &&
sudo dnf group upgrade --with-optional Multimedia &&
sudo dnf update
